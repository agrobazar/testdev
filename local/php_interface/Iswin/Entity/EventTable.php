<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Entity;


use Iswin\Controller\LangController;
use Iswin\Iblock\ElementPropertyTable;

/**
 * класс для работы с сущностью мероприятий
 *
 * Class EventTable
 * @package Iswin\Entity
 */
class EventTable extends ElementPropertyTable
{

    protected static function getIblockId()
    {
        return LangController::getInstance()->getEventsIblockId();
    }

}