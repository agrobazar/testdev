<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Entity;

use Bitrix\Highloadblock as HL;
use Iswin\Settings\HlBlock;

/**
 * КЛасс фабрика для Highload блока - рубрики
 *
 * Class Category
 * @package Iswin\Entity
 */
class Category
{
    protected static $entityClass = false;

    /**
     * @return \Bitrix\Main\ORM\Data\DataManager
     */
    public static function getEntity()
    {
        if (self::$entityClass !== false) {
            return self::$entityClass;
        }

        $hlblock   = HL\HighloadBlockTable::getById(HlBlock::getCategoryEntityId())->fetch();
        $entity   = HL\HighloadBlockTable::compileEntity( $hlblock ); //генерация класса
        $entityClass = $entity->getDataClass();
        return self::$entityClass = $entityClass;
    }

}