<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Settings;

/**
 * Класс, содержащий настройки инфоблока
 *
 * Class Iblock
 * @package Iswin\Settings
 */
class Iblock
{
    const SPEAKERS_CODE = 'speakers';
    const EVENTS_CODE = 'events';
    const PUBLICATIONS_CODE = 'publications';

    /**
     * Возвращает код инфоблока спикеров
     *
     * @return string
     */
    public static function getSpeakersCode()
    {
        return self::SPEAKERS_CODE;
    }

    /**
     * Возвращает код инфоблока мероприятий
     *
     * @return string
     */
    public static function getEventsCode()
    {
        return self::EVENTS_CODE;
    }

    /**
     * Возвращает код инфоблока публикаций
     *
     * @return string
     */
    public static function getPublicationsCode()
    {
        return self::PUBLICATIONS_CODE;
    }
}