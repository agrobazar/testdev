<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Settings;

/**
 * Содержит настройки хайлоад блоков.
 *
 * Class HlBlock
 * @package Iswin\Settings
 */
class HlBlock
{
    const CATEGORY_NAME_FIELD_CODE = 'UF_NAME';

    const CATEGORY_ENTITY_ID = 1;

    /**
     * Возвращает код поля содержащеего имя категории
     *
     * @return string
     */
    public static function getCategoryNameFieldCode()
    {
        return self::CATEGORY_NAME_FIELD_CODE;
    }

    /**
     * Возвращает ID highload блока рубрик
     *
     * @return int
     */
    public static function getCategoryEntityId()
    {
        return self::CATEGORY_ENTITY_ID;
    }
}