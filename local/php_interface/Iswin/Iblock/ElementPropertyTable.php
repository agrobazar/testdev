<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Iblock;

use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity;

/**
 * базовый абстрактный класс для работы с единичными свойствами элементов инфоблока в d7
 *
 * Class ElementPropertyTable
 * @package Iswin\Iblock
 * @internal не использовать класс без наследников
 */
abstract class ElementPropertyTable extends DataManager
{


    protected static $iblockCache = [];

    /**
     * Должен быть определен в наследнике и возвращать ID инфоблока
     *
     * @return mixed
     */
    abstract protected static function getIblockId();

    /**
     * Возвращает ассоциативный массив свойств
     *
     * @return array
     */
    protected static function getProperties()
    {
        $iblockId = static::getIblockId();

        if (isset(self::$iblockCache[$iblockId])) {
            return self::$iblockCache[$iblockId];
        }


        $rows = PropertyTable::query()
            ->addSelect('ID')
            ->addSelect('PROPERTY_TYPE')
            ->addSelect('CODE')
            /** Данный класс не работает с множественными значениями */
            ->addFilter('!MULTIPLE', true)
            ->addFilter('=IBLOCK_ID', $iblockId)
            ->exec();

        $ret = [];

        while ($row = $rows->fetch()) {
            $ret[$row['CODE']] = $row;
        }

        return self::$iblockCache[$iblockId] = $ret;
    }

    /**
     * Возвращает свойство по его коду
     *
     * @param $code
     * @return mixed
     */
    public static function getPropertyByCode($code)
    {
        return static::getProperties()[$code];
    }

    /**
     * Возвращает название поля для свойства по его коду
     * используется для включения конкретного свойства в queryBuilder
     * <code>
     *  IblockElement::query()
     *      ->addSelect(EventTable::translateField('PROPS.SPEAKER')) //translateField вернет PROPS.PROPERTY_5
     * </code>
     *
     * @param $string
     * @return string
     */
    public static function translateField($string)
    {
        list($alias, $code) = explode('.', $string);
        return "{$alias}.PROPERTY_" . self::getPropertyByCode($code)['ID'];
    }

    /**
     * Формирует список полей для функции getMap в сущностях
     * @see DataManager::getMap()
     *
     * @return mixed
     */
    protected static function makePropMap()
    {

        $props = static::getProperties();

        $ret = [];

        foreach ($props as $row) {
            $type = $row['PROPERTY_TYPE'];

            switch ($type) {
                case PropertyTable::TYPE_NUMBER:
                case PropertyTable::TYPE_ELEMENT:
                case PropertyTable::TYPE_SECTION:
                    $ret[] = new Entity\IntegerField("PROPERTY_{$row['ID']}");
                    break;
                default:
                    $ret[] = new Entity\StringField("PROPERTY_{$row['ID']}");
            }
        }


        return $ret;
    }

    /**
     * @see DataManager::getTableName()
     *
     * @return string
     */
    public static function getTableName ()
    {
        $baseTableName = "b_iblock_element_prop_s";
        $iblockId = static::getIblockId();
        return $baseTableName . $iblockId;
    }


    /**
     * @see DataManager::getMap()
     *
     * @return array
     */
    public static function getMap ()
    {
        $map = [
            new Entity\IntegerField('IBLOCK_ELEMENT_ID',
                [
                    'primary' => true
                ]
            ),
            new Entity\ReferenceField(
                'ELEMENT',
                '\Bitrix\Iblock\Element',
                array('=this.IBLOCK_ELEMENT_ID' => 'ref.ID')
            )
        ];

        $propMap = static::makePropMap();
        return array_merge($map, $propMap);
    }
}