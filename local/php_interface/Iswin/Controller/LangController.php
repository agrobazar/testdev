<?php
/**
 * Created by v.taneev.
 */


namespace Iswin\Controller;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Iswin\Settings\HlBlock;
use Iswin\Settings\Iblock;


/**
 * Класс для работы с языкозависимыми данными
 *  В задании не было указано, что нужно использовать механизмы 1С-Битрикс при работе с языками,
 *  поэтому для упращения задачи был создан этот контроллер.
 *
 * Class LangController
 * @package Iswin\Services
 */
class LangController
{
     const AVAIL_LANGS = [
             ['code' => 'ru', 'name' => 'Русский'],
             ['code' => 'en', 'name' => 'English']
     ];

    protected static $instance = false;

    protected $currentLang = false;

    /**
     * Задачи кешировать данные с помощью битрикса не было,
     * однако, нужно предотвратить повторные выборки одних и тех данных, хотя бы в рамках одного хита
     * в идеале конечно кешировать битриксом.
     *
     * @var array
     */
    protected static $cacheIblockIds = [];

    protected function __construct ()
    {
    }

    /**
     * Возвращает инстанс контроллера, если он еще не создал - создает его
     *
     * @return bool|LangController
     */
    public static function getInstance()
    {
        if (self::$instance !== false) {
            return self::$instance;
        }

        return self::$instance = self::createInstance();
    }

    /**
     * Создает инстанс контроллера
     *
     * @return static
     */
    public static function createInstance()
    {
        return new static();
    }

    /**
     * Возвращает ассоциативный массив доступных языков
     *
     * @return array
     */
    public function getAvailLanguages()
    {
        $availLangs = self::AVAIL_LANGS;
        $currentLang = $this->getCurrentLang();
        foreach ($availLangs as &$row) {
            $row['active'] = ($row['code'] == $currentLang['code']);
        }

        return $availLangs;
    }

    /**
     * Возвращает ассоциативный язык текущего языка пользователя
     *
     * @return bool|mixed
     */
    public function getCurrentLang()
    {
        if ($this->currentLang !== false) {
            return $this->currentLang;
        }

        if ($_SESSION['USER_LANG']) {
            return $this->currentLang = $_SESSION['USER_LANG'];
        }

        $langs = self::AVAIL_LANGS;
        reset($langs);
        return $this->currentLang = $_SESSION['USER_LANG'] = current($langs);
    }

    /**
     * Устанавливает язык по его коду
     *
     * @param $languageCode
     * @return bool
     */
    public function setLanguage($languageCode)
    {
        $languageCode = strtolower($languageCode);
        foreach ($this->getAvailLanguages() as $row) {
            if (strtolower($row['code']) == $languageCode) {
                $this->currentLang = $_SESSION['USER_LANG'] = $row;
                return true;
            }
        }
        return false;
    }

    /**
     * Возвращает код поля содержащего название категории, для
     * текущего языка пользователя
     *
     * @return string
     */
    public function getCategoryFieldName()
    {
        $currentLangCode = strtoupper($this->getCurrentLang()['code']);
        return HlBlock::getCategoryNameFieldCode() . "_" . $currentLangCode;
    }

    /**
     * Возвращает ID инфоблока спикеров для выбранного языка
     *
     * @return int
     */
    public function getSpeakersIblockId()
    {
        return $this->getIblockIdByBaseCode(Iblock::getSpeakersCode());
    }

    /**
     * Возвращает ID инфоблока мероприятий для выбранного языка
     *
     * @return int
     */
    public function getEventsIblockId()
    {
        return $this->getIblockIdByBaseCode(Iblock::getEventsCode());
    }

    /**
     * Возвращает ID инфоблока публикаций для выбранного языка
     *
     * @return int
     */
    public function getPublicationsIblockId()
    {
        return $this->getIblockIdByBaseCode(Iblock::getPublicationsCode());
    }

    /**
     * Возвращает ID инфоблока по его коду
     *
     * @param $code
     * @return int
     */
    protected function getIblockIdByCode($code)
    {
        if (isset(static::$cacheIblockIds[$code])) {
            return static::$cacheIblockIds[$code];
        }

        Loader::includeModule('iblock');
        $row = IblockTable::query()
            ->addSelect('ID')
            ->addFilter('=CODE', $code)
            ->exec()
            ->fetch();

        return static::$cacheIblockIds[$code] = $row['ID'];
    }

    /**
     * Возвращает ID инфоблока по его базовому коду для выбранного языка
     *
     * @param $baseCode
     * @return int
     */
    protected function getIblockIdByBaseCode($baseCode)
    {
        $langCode = strtolower($baseCode) . "_" . strtolower($this->getCurrentLang()['code']);
        return $this->getIblockIdByCode($langCode);
    }

}