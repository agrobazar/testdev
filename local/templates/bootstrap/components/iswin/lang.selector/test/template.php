<?php
/**
 * Created by v.taneev.
 *
 * @var $arResult
 * @var $arParams
 */


$languages = $arResult['languages'];
if (!$languages) {
    return;
}

?>


<ul class="navbar-nav mr-auto">
    <?foreach($arResult['languages'] as $lang):?>
        <li class="nav-item <?if($lang['active']):?>active<?endif;?>">
            <a class="nav-link" href="<?=$lang['url']?>"><?=$lang['name']?><?if($lang['active']):?> <span class="sr-only">(current)</span><?endif;?></a>
        </li>
    <?endforeach;?>
</ul>
