<?php
/**
 * Created by v.taneev.
 */

?>

<div class="card">
    <div class="card-body">
        <form action="" method="post" class="filter-form">
            <input type="hidden" name="act" value="send" />
            <?foreach($arResult['categories'] as $category):?>
                <label <?if($category['active']):?>class="active"<?endif;?>>
                    <input type="checkbox" name="category[]" <?if($category['active']):?>checked<?endif;?> value="<?=$category['value']?>">
                    <?=$category['name']?>
                    <span class="count">(<?=$category['count']?>)</span>
                </label>
            <?endforeach;?>
        </form>
    </div>
</div>

<hr>
<ol class="list-group">
    <?foreach($arResult['speakers'] as $arItem):?>
        <li class="list-group-item">#<?=$arItem['id']?> <?=$arItem['name']?></li>
    <?endforeach;?>
</ol>