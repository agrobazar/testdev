$(function() {

    initListActions();
});


function initListActions() {
    $('.filter-form label').on('click', function(e) {
        e.preventDefault();
        var $form = $(this).closest('form');
        var $inp = $('input[type="checkbox"]', $(this));
        if ($(this).hasClass('active')) {
            $inp.prop('checked', false).attr('checked', false);
            $(this).removeClass('active');
        } else {
            $inp.prop('checked', true).attr('checked', true);
            $(this).addClass('active');
        }
        $form.submit();
    });
}