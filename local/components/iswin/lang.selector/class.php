<?php
/**
 * Created by v.taneev.
 */

use Bitrix\Main\Application;
use Iswin\Controller\LangController;

/**
 * Компонент для вывода переключателя языков
 *
 * Class IswinLangSelector
 */
class IswinLangSelector extends \CBitrixComponent {

    /**
     * Гет параметр для установки языка,
     * конечно правильней вынести в параметры компонента, но в рамках тестовой задачи не имеет смысла
     */
    const SET_LANG_PARAM = 'setLang';

    /**
     * @return \Bitrix\Main\HttpRequest
     */
    protected function getRequest()
    {
        return Application::getInstance()->getContext()->getRequest();
    }

    /**
     * @return bool|LangController
     */
    protected function getLangController()
    {
        return LangController::getInstance();
    }

    public function executeComponent ()
    {
        /** Обработка - установка языка */
        if ($langCode = $this->getRequest()->get('setLang')) {
            global $APPLICATION;
            $this->getLangController()->setLanguage($langCode);
            LocalRedirect($APPLICATION->GetCurPageParam("", [self::SET_LANG_PARAM]), false, 301);
            return;
        }

        $this->loadLanguages();
        $this->includeComponentTemplate();
    }

    /**
     * Загружает доступные языки в arResult
     */
    protected function loadLanguages()
    {
        global $APPLICATION;
        $availLangs = $this->getLangController()->getAvailLanguages();
        foreach ($availLangs as &$row) {
            $param = self::SET_LANG_PARAM . "=" . $row['code'];
            $row['url'] = $APPLICATION->GetCurPageParam($param, [self::SET_LANG_PARAM]);
        }

        $this->arResult['languages'] = $availLangs;
    }

}