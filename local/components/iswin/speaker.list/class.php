<?php
/**
 * Created by v.taneev.
 */

use Bitrix\Main\Loader;
use Bitrix\Iblock\ElementTable;
use Iswin\Entity\EventTable;
use Iswin\Entity\PublicationTable;
use Iswin\Controller\LangController;
use \Iswin\Entity\Category;
use Bitrix\Main\Application;

/**
 * Компонент для выода списка спикеров с фильтрацией по рубрикам
 *
 * Class IswinSpeakerList
 */
class IswinSpeakerList extends CBitrixComponent {


    /**
     * @return \Bitrix\Main\HttpRequest
     */
    protected function getRequest()
    {
        return Application::getInstance()->getContext()->getRequest();
    }


    /**
     * Возвращает выбранные категории
     *
     * @return array
     */
    protected function getSelectedCategory()
    {
        return $this->getRequest()->get('category') ? : [];
    }

    public function executeComponent ()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');

        $this->loadCategory();
        $this->loadSpeakers();
        $this->includeComponentTemplate();
    }

    /**
     * Загружает в arResult отфильтрованный список спикеров
     */
    protected function loadSpeakers()
    {

        $filter = [
            '=IBLOCK_ID' => LangController::getInstance()->getSpeakersIblockId(),
            '=ACTIVE' => 'Y'
        ];

        $selectedCategories = $this->getSelectedCategory();


        if ($selectedCategories) {
            $filter[] = [
                'LOGIC' => 'OR',
                [EventTable::translateField('=PROPS_EVENT.CATEGORY') => $selectedCategories],
                [PublicationTable::translateField('=PROPS_PUBLICATION.CATEGORY') => $selectedCategories],
            ];
        }


        $rows = ElementTable::query()
            ->addSelect('ID')
            ->addSelect('NAME')
            ->setFilter($filter)
            ->registerRuntimeField("PROPS_EVENT",
                [
                    "data_type"   => '\Iswin\Entity\Event',
                    'reference'    => array('=this.ID' => EventTable::translateField('ref.SPEAKER')),
                ]
            )
            ->registerRuntimeField("PROPS_PUBLICATION",
                [
                    "data_type"   => '\Iswin\Entity\Publication',
                    'reference'    => array('=this.ID' => PublicationTable::translateField('ref.SPEAKER')),
                ]
            )
            ->addGroup('ID')
            ->exec();

        $ret = [];
        while ($row = $rows->fetch()) {
            $ret[] = [
                'id' => $row['ID'],
                'name' => $row['NAME']
            ];
        }

        $this->arResult['speakers'] = $ret;
    }


    /**
     * Загружает все категории в arResult
     */
    protected function loadCategory()
    {

        $category = Category::getEntity();
        $rows = $category::query()
            ->addSelect('ID')
            ->addSelect('UF_XML_ID', 'XML_ID')
            ->addSelect(LangController::getInstance()->getCategoryFieldName(), 'NAME')
            ->addSelect(EventTable::translateField('PROPS_EVENT.SPEAKER'), 'SPEAKER_EVENT_ID')
            ->addSelect(PublicationTable::translateField('PROPS_PUBLICATION.SPEAKER'), 'SPEAKER_PUBLICATION_ID')
            ->registerRuntimeField("PROPS_EVENT",
                [
                    "data_type"   => '\Iswin\Entity\Event',
                    'reference'    => array('=this.UF_XML_ID' => EventTable::translateField('ref.CATEGORY')),
                ]
            )
            ->registerRuntimeField("PROPS_PUBLICATION",
                [
                    "data_type"   => '\Iswin\Entity\Publication',
                    'reference'    => array('=this.UF_XML_ID' => PublicationTable::translateField('ref.CATEGORY')),
                ]
            )->exec();


        $ret = [];
        $speakersCounters = [];

        $filterCategories = $this->getSelectedCategory();

        /**
         * т.к. спикеры публикаций и событий лежат в разных таблицах, то не представляется возможным возможным, работая с d7,
         * посчитать сразу общее количество уникальных спикеров для каждой категории события и публикации,
         * мы все выбрали одним запросом, но на выходе у нас получалась матрица, требующая обработки
         */
        while ($row = $rows->fetch()) {

            $categoryId = $row['ID'];

            if (!isset($ret[$categoryId])) {
                $ret[$categoryId] = [
                    'id' => $row['ID'],
                    'value' => $row['XML_ID'],
                    'name' => $row['NAME'],
                    'active' => in_array($row['XML_ID'], $filterCategories),
                    'count' => 0
                ];

                $speakersCounters[$categoryId] = [];
            }

            $eventSpeakerId = $row['SPEAKER_EVENT_ID'];
            /** isset работает быстрее чем in_array */
            if ($eventSpeakerId && !isset($speakersCounters[$categoryId][$eventSpeakerId])) {
                $speakersCounters[$categoryId][$eventSpeakerId] = $eventSpeakerId;
                $ret[$categoryId]['count']++;
            }

            $publicationSpeakerId = $row['SPEAKER_PUBLICATION_ID'];
            if ($publicationSpeakerId && !isset($speakersCounters[$categoryId][$publicationSpeakerId])) {
                $speakersCounters[$categoryId][$publicationSpeakerId] = $publicationSpeakerId;
                $ret[$categoryId]['count']++;
            }
        }

        $this->arResult['categories'] = array_filter($ret, function($row) {
            return $row['count'] > 0;
        });


    }


}